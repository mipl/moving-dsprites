# Moving dSprites

The Moving dSprites dataset is built on the basis of the [Deepmind's dSprites dataset](https://github.com/deepmind/dsprites-dataset).

This library contains the Python code to generate videos of the moving shapes, following controllable trajectories.

## Factors of Variation

The generated videos has the following factors of variation:

| Symbol      | Description                       | Possible values (inclusive ranges) |
| ----------- | --------------------------------- |:----------------------------------:|
| R           | Red contribution on shape color   | 0-255                              |
| G           | Green contribution on shape color | 0-255                              |
| B           | Blue contribution on shape color  | 0-255                              |
| x0          | Initial x position                | 0-31                               |
| y0          | Initial y position                | 0-31                               |
| xf          | Final x position                  | 0-31                               |
| yf          | Final y position                  | 0-31                               |
| orientation | Orientation of the shape          | 0-39                               |
| shape       | Shape (as original dataset)       | 0-2                                |
| scale       | Scale (as original dataset)       | 0-5                                |
| speed       | Speed of motion                   | 1-3                                |
| t           | Type of trajectory                | 'linear', 'arc'                    |

Most of the factors may result familiar to the ones acquainted to the original dataset.
The novel factors correspond to the color of the shape (R,G,B), the motion speed,
which is attained by simply uniformly sampling frames of a resulting video, in
order to make it look faster. Finally, our code generates two types of trajectories,
given the initial and final coordinates: the linear trajectory is straightforward,
while the arc trajectory makes the shape move from (x0,y0) to (xf,yf) following
on arc of the circle where (x0,y0) and (xf,yf) are diametrically opposed.

### Background and Minimum Number of Frames

The user can control the intensity of the background at grayscale, and the
minimum number of frames, so trivial videos are discarded at generation time.

## Formats

You can choose between mp4, GIF and PNG format to store the videos. The PNG
format corresponds to mosaic images where the frames are in either horizontal,
vertical or matrix (default) tiles, as shown in the examples bellow.

PNG

Creates a matrix mosaic of $`H \times W`$ where

```python
# f : number of frames
H = int(np.sqrt(f))
W = int(np.ceil(f / H))
```

<img src="examples/100_185_90_2_3_12_23_15_17_24_arc_3.png">
<img src="examples/89_10_31_0_1_2_28_5_21_2_arc_2.png">

PNG horizontal

<img src="examples/100_185_90_2_3_12_23_15_17_24_arc_3_h.png">

<img src="examples/89_10_31_0_1_2_28_5_21_2_arc_2_h.png">

PNG vertical

<img src="examples/100_185_90_2_3_12_23_15_17_24_arc_3_v.png">
<img src="examples/89_10_31_0_1_2_28_5_21_2_arc_2_v.png">

GIF

<img src="examples/100_185_90_2_3_12_23_15_17_24_arc_3.gif">
<img src="examples/89_10_31_0_1_2_28_5_21_2_arc_2.gif">

mp4

<a href="examples/100_185_90_2_3_12_23_15_17_24_arc_3.mp4">Example 1</a>

<a href="examples/89_10_31_0_1_2_28_5_21_2_arc_2.mp4">Example 2</a>


## Dependencies

Before running the code, you must have the `imageio`, `numpy`, `argparse`,
`tqdm`, `wget`, and `imageio-ffmpeg` Pythonlibraries.

```sh
$ pip install -r requirements.txt
```

## Usage

```sh
$ python main.py --help
usage: main.py [-h] [-n NUM_EXAMPLES] [-f MIN_FRAMES] [-s RANDOM_STATE]
               [-b BACKGROUND] [-r {gif,mp4,png,pngh,pngv}]
               output_path

Generation of the Moving-dSprites dataset

positional arguments:
  output_path           Output path.

optional arguments:
  -h, --help            show this help message and exit
  -n NUM_EXAMPLES, --num_examples NUM_EXAMPLES
                        Number of videos to be generated.
  -f MIN_FRAMES, --min_frames MIN_FRAMES
                        Minimum number of frames per video.
  -s RANDOM_STATE, --random_state RANDOM_STATE
                        Random state.
  -b BACKGROUND, --background BACKGROUND
                        Background intensity.
  -r {gif,mp4,png,pngh,pngv}, --format {gif,mp4,png,pngh,pngv}
                        Format of the output videos (png format are images
                        whose frames are in an horizontal, vertical or matrix
                        tile).
```

Examples:

```sh
$ python main.py ./vids/ -b 0                       # To produce 10000 PNG matrix videos with black background in ./vids
$ python main.py ./vids/ -b 255 -r gif              # To produce 10000 GIFs videos with white background.
$ python main.py ./vids/ -b 100 -n 50 -f 15 -r pngv # To produce 50 PNG vertical videos with gray background and with >=15 frames.
```

Check the `sample_factors` function at `dsprites.py` in order to see how to
personalize the generation process, by fixing the factors you want.

### List of Factors

A file named `list` will also be generated, in order to associate each video to
its factors of variation and number of frames. Each line in the file has the
following format:

```
<file name> <num_frames> <R> <G> <B> <shape> <scale> <orientation> <x0> <y0> <xf> <yf> <t> <speed>
```

## Contributors

The trajectory generator was developed by Arthur Costa Lopes, and the interface and video writers was developed by Juan Hernández Albarracín. Both contributors are from University of Campinas, Brazil.