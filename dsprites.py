import numpy as np
import os, wget, math
from numpy.random import randint

TRAJS = ['linear', 'arc']

class dSprites:

    def __init__(self):

        d_path = 'dsprites_ndarray_co1sh3sc6or40x32y32_64x64.npz'
        if not os.path.exists(d_path):
            url = 'https://raw.githubusercontent.com/deepmind/dsprites-dataset/master/dsprites_ndarray_co1sh3sc6or40x32y32_64x64.npz'
            print(d_path + ' file not found. Downloading from ' + url)
            wget.download(url, './')

        dataset_zip = np.load(d_path, encoding = "latin1", allow_pickle = True)

        print('Loading images...')

        self.imgs            = dataset_zip['imgs']
        self.latents_values  = dataset_zip['latents_values']
        self.latents_classes = dataset_zip['latents_classes']
        self.metadata        = dataset_zip['metadata'][()]

        self.latents_sizes = self.metadata['latents_sizes']
        self.lat_bases = np.concatenate((
            self.latents_sizes[::-1].cumprod()[::-1][1:], np.array([1,])))

    def sample_factors(self, fixed = {}):

        R     = fixed['R']     if   'R'   in fixed else randint(0, 256)
        G     = fixed['G']     if   'G'   in fixed else randint(0, 256)
        B     = fixed['B']     if   'B'   in fixed else randint(0, 256)
        x0    = fixed['x0']    if   'x0'  in fixed else randint(0, 32)
        y0    = fixed['y0']    if   'y0'  in fixed else randint(0, 32)
        xf    = fixed['xf']    if   'xf'  in fixed else randint(0, 32)
        yf    = fixed['yf']    if   'yf'  in fixed else randint(0, 32)
        shape = fixed['shape'] if 'shape' in fixed else randint(0, 3)
        scale = fixed['scale'] if 'scale' in fixed else randint(0, 6)
        speed = fixed['speed'] if 'speed' in fixed else randint(1, 4)
        t     = fixed['t']     if   't'   in fixed else TRAJS[randint(0, 2)]
        
        orientation = fixed['orientation'] if 'orientation' in fixed \
                                        else randint(0, 40)
        
        return (R, G, B, shape, scale, orientation, x0, y0, xf, yf, t, speed)

    def generate_trajectory(self, R, G, B, shape, scale, orientation,
                        x0, y0, xf, yf, traj, speed, Bg = 0, min_frames = 5):
    
        lat2index = lambda latents : np.dot(latents, self.lat_bases).astype(int)

        if x0 == xf and y0 == yf:
            return None
        
        if isinstance(Bg, int):
            if Bg == R and R == G and G == B:
                return None
        elif Bg[0] == R and Bg[1] == G and Bg[2] == B:
            return None
        
        if traj == 'linear':
            frames = max(abs(xf-x0), abs(yf-y0))
            y_step = math.ceil(frames/(max(abs(yf-y0), 1)))
            x_step = math.ceil(frames/(max(abs(xf-x0), 1)))
            y_step_cur, x_step_cur = (y_step, x_step)
        elif traj == 'arc':
            theta_step = np.deg2rad(10)
            radius     = np.sqrt((xf-x0)**2 + (yf-y0)**2)/2
            x_c        = min(x0, xf) + np.abs(x0-xf)/2
            y_c        = min(y0, yf) + np.abs(y0-yf)/2
            theta_0    = np.arccos((abs(x_c-x0))/radius)
            if (x0 < x_c and y0 < y_c):
                theta_0 += np.pi/2
            elif (x0 < x_c and y0 > y_c):
                theta_0 += np.pi
            elif(x0 > x_c and y0 > y_c):
                theta_0 += 1.5*np.pi
    
        x, y     = (x0, y0)
        img      = self.imgs[lat2index([0, shape, scale, orientation, x0, y0])]
        cur_imgs = []
    
        if traj == 'linear':
            for _ in range(frames):
                if x_step_cur == 1:
                    if abs(xf-x) > 0:
                        x += np.sign(xf-x0)
                        x_step_cur = x_step
                else:
                    x_step_cur -= 1
    
                if y_step_cur == 1:
                    if abs(yf-y) > 0:
                        y += np.sign(yf-y0)
                        y_step_cur = y_step
                else:
                    y_step_cur -= 1
                
                cur_imgs.append(self.imgs[lat2index(
                                        [0, shape, scale, orientation, x, y])])
    
        elif traj == 'arc':
            theta     = theta_0
            cum_theta = 0
            while cum_theta < np.deg2rad(180):
                theta     += theta_step
                if theta >= 2*np.pi:
                    theta -= 2*np.pi
                cum_theta += theta_step
                old_x, old_y = (x, y)
                x = radius*np.cos(theta) + x_c
                y = radius*np.sin(theta) + y_c
                if int(old_x) == int(x) and int(old_y) == int(y):
                    continue
                if x < 0 or y < 0 or x > 31 or y > 31:
                    return None
            
                cur_imgs.append(self.imgs[lat2index(
                                [0, shape, scale, orientation, int(x), int(y)])])
        
        cur_imgs = cur_imgs[::speed]
    
        if len(cur_imgs) + 1 < min_frames:
            return None
    
        img = np.stack([img] + cur_imgs)
        img = np.repeat(img[..., None], 3, axis = -1)
        img *= np.array([[[[R, G, B]]]], dtype = img.dtype)
        iBg = np.zeros_like(img)
        mask = np.prod(img == iBg, axis = 3)
        img[np.where(mask)] = Bg
    
        return img