from writer import VideoWriter, check_and_create_path
from argparse import ArgumentParser
from dsprites import dSprites
from tqdm import tqdm
import numpy as np
import os

if __name__ == '__main__':

    parser = ArgumentParser(
                    description = 'Generation of the Moving-dSprites dataset')

    parser.add_argument('-n', '--num_examples',
        help = "Number of videos to be generated.",
        type = int,
        default = 10000)

    parser.add_argument('-f', '--min_frames',
        help = "Minimum number of frames per video.",
        type = int,
        default = 5)

    parser.add_argument('output_path',
        help = 'Output path.',
        type = str)

    parser.add_argument('-s', '--random_state',
        help = "Random state.",
        type = int)

    parser.add_argument('-b', '--background',
        help = "Background intensity.",
        type = int,
        default = 0)

    parser.add_argument('-r', '--format',
        help = 'Format of the output videos (png format are images whose ' + 
                    'frames are in an horizontal, vertical or matrix tile).',
        type = str,
        choices = ['gif', 'mp4', 'png', 'pngh', 'pngv'],
        default = 'png')

    args = parser.parse_args()

    if args.random_state is not None:
        np.random.seed(args.random_state)

    ds = dSprites()
    sv = VideoWriter(args.format)
    files = []

    fixed_factors = {}

    # To generate, for example, only red hearts:
    # fixed_factors = {'R' : 255, 'G' : 0, 'B' : '0', 'shape' : 2}

    check_and_create_path(args.output_path)

    fout = open(os.path.join(args.output_path, 'list'), 'w')
    for i in tqdm(range(args.num_examples), desc = 'Generating videos...'):

        img = None
        while img is None:

            s = ds.sample_factors(fixed_factors)
            if s in files:
                continue

            img = ds.generate_trajectory(*s, args.background, args.min_frames)
        
        files += [s]
        file_name  = '_'.join(['{}'] * 12).format(*s)
        file_facts = ' '.join(['{}'] * 12).format(*s)
        file_name = file_name + '.' + sv.save(img, args.output_path, file_name)
        fout.write('%s %d %s\n' % (file_name, img.shape[0], file_facts))

    fout.close()


